from django.shortcuts import render, redirect, get_object_or_404
from recipes.models import Recipe
from recipes.forms import RecipeForm


def show_all_recipes(request):
    # goal of "context" is a dictionary of keys and return the
    # . value of said keys
    recipes = Recipe.objects.all()
    context = {
        "recipes": recipes,
    }
    return render(request, "recipes/list.html", context)

def show_recipe(request, id):
    recipe = Recipe.objects.get(id=id)
    context = {
        "recipe": recipe,
    }
    return render(request, "recipes/detail.html", context)

def create_recipe(request):
    if request.method == "POST":
        # Validate values with form and save to DB
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            # If it works, we can redirect the browser
            #  back to main and leave it
            return redirect("show_all_recipes")
    else:
        form = RecipeForm()
        context = {
            "recipe": form,
        }
        return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    # recipe = Recipe.objects.get(id=id)
    if request.method == "POST":
        # Validate values with form and save to DB
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            # If it works, we can redirect the browser
            #  back to main and leave it
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)

        context = {
            "recipe": recipe,
            "recipe_form": form,
        }
        return render(request, "recipes/edit.html", context)
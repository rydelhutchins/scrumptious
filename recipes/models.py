from django.db import models

class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    rating = models.FloatField(default=0.0)
    created_on = models.DateTimeField(auto_now_add=True)